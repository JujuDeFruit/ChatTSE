class User {
    // User contructor
    constructor(u, r){
        this.username = u;
        this.room = r;
    }

    // Equivalent of User copy constructor to cast parsed User to User.
    static create(user) { return new User(user.username, user.room); }
}


class Message {
    // Message constructor.
    constructor(user_, message_, date_) {
        this.user = user_;
        this.message = message_;
        this.date = date_;
    }

    // Equivalent of Message copy constructor to cast parsed Message to Message.
    static create(msg) { return new Message(msg.user, msg.message, msg.date); }

    // Print message in chat.
    addMessage() {
        // Clone message template.
        var clone = $("#msgtpl").clone();

        // Add username.
        clone.find("#username >").append(this.user.username + ": ");

        // Add message and reset form.
        clone.find("#message").append(this.message);
        $("#form > #messageToSend").val("");

        // Add date.
        clone.find("#date").append(this.date);

        // Append filled message template to DOM and show it (hidden by default).
        clone.appendTo("#messages").show();
    }
}