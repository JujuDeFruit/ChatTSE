// In this file, JQuery on document ready does not fit with socket.io that is why there is not document ready event.
// Moreover, we did not figure out how to require modules on client-side to use User() and Message() classes.
// To fix this, we add utils.js script to html file.

var socket = io.connect();

// Current user.
var user;

// On login form send event.
// Form is always send at least twice. 
// First, sending is canceled and client check server to know if user already exists or not.
// If user already exists then do not send login else send login.
// This form is conditionned by 'userExists' event.
$('#loginform').submit((e) => {
    // Hidden input that contains bool to know if form can be send or not.
    let preventDefault = $('#loginform > #preventDefault').val();
    // Get user infos.
    username = $('#username').val();
    room = $("#room").val();
    user = new User(username, room);
    // Act following hidden boolean.
    if (preventDefault == "true") {
        // Cancel form sending and test if user already exists or not.
        e.preventDefault();
        socket.emit('login', { user: user });
    } else if(preventDefault == "false") {
        // User did not exist already, then it was just added to users and alert other there is a new user.
        socket.emit('newUser_', user);
    }
})

// Alert about new user.
socket.on('newUser', (newUser) => { 
    // Print connection toast.
    $("#toast-title").text("").append("Connection");
    $("#toast-body").text("").append(newUser.username + " just connected to room n°" + newUser.room) 
    $('.toast').toast('show');
});

// Alert about disconnecting user.
socket.on('rmUser', (user) => {
    // Print disconnection toast.
    $("#toast-title").text("").append("Disconnection");
    $("#toast-body").text("").append(user.username + " just disconnected"); 
    $('.toast').toast('show');
});

// Get the answer from the server if user already existed or not.
socket.on("userExists", (bool_) => {
    if (bool_) {
        // User already existed.
        $('.alert').css('display', 'block');
    } else {
        // It is a new user.
        // Change hidden input value to false and submit form again. Thsi time, preventDefault boolean will be false.
        $('#loginform > #preventDefault').val("false");
        $('#loginform').trigger("submit");
    }
});
