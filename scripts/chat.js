// In this file, JQuery on document ready does not fit with socket.io that is why there is not document ready event.
// Moreover, we did not figure out how to require modules on client-side to use User() and Message() classes.
// To fix this, we add utils.js script to html file.

var socket = io.connect();

// Return user from http request URL.
function getUser() {
    let url = new URL(document.location.href);
    username = url.searchParams.get("login");
    room = url.searchParams.get("room");

    let user = new User(username, room);

    return user;
}

// Get current user.
var user = getUser();

// Hide template of message. We will clone it further to print message.
$("#msgtpl").hide();

// Get current users in teh room from server.
socket.emit('getUsers');
// Load previous messages from chat.
socket.emit('getChat', 5);

// On sending message.
$("#form").submit((e) => {
    // Do not refresh the page.
    e.preventDefault();

    // Get message content.
    let message = $("#form > #messageToSend").val();

    // Get message sending time.
    let date = new Date();
    let minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    let date_ = date.getHours() + ":" + minutes;

    // Create a new Message.
    let msg = new Message(user, message, date_);
    // Print the message in the chat.
    msg.addMessage();

    // Send message to server to aware all users in the same room there is a new messsage.
    socket.emit("send", msg);
});

// On log out, remove user from the server.
$("#logOut").on("submit", () => {
    socket.emit('userLogOut', user);
});

// When there is a new user
socket.on('newUser', (newUser) => {
    // If new user is in the same room than current user, print new user in users list. 
    newUser.room == user.room ? $("#listusers").append('<li class="list-group-item">' + newUser.username + '</li>').addClass("list-group-item") : null;
    // Create alert.
    // Alert about new user.
    // Print connection toast.
    $("#toast-title").text("").append("Connection");
    $("#toast-body").text("").append(newUser.username + " just connected to room n°" + newUser.room)
    $('.toast').toast('show');
});

// On other user log out.
socket.on('rmUser', (user_) => {
    // Browse users list and remove user if this one match the disconnected user.
    $("#listusers").children('li').each((index, jQuery) => {
        $(jQuery).text() == user_.username && user.room == user_.room ? $(jQuery).remove() : null;
        // Print disconnection toast.
        $("#toast-title").text("").append("Disconnection");
        $("#toast-body").text("").append(user_.username + " just disconnected");
        $('.toast').toast('show');
    });
});

// Print all users in the same room on page refresh.
socket.on('setUsers', (users) => {
    // Cast to array current users.
    users_ = JSON.parse(users);
    users_.forEach(element => {
        // Cast element to User.
        let u = User.create(element);
        // If current user is in the same room than user, print it.
        user.room == u.room ? $("#listusers").append('<li class="list-group-item">' + element.username + '</li>') : null;
    });
});

// Get the new message send by other user or all messages in the server on page refresh.
socket.on('msg', (msg) => {
    // Cast the string.
    messages = JSON.parse(msg);
    messages.forEach(m => {
        // For each message casts it to Message type.
        let message = Message.create(m);
        // Print message only if user that sent it is in the same room of the current one.
        message.user.room == user.room ? message.addMessage() : null;
    })
});