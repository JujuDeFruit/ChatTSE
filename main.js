const express = require("express");
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const swig = require('swig');
const path = require('path');

const PORT = 8080;

// Arrays to store users and messages
// A user has a username and is affected to a room.
// A message has a user sender, a content and an hour.
var users = [];
var messages = [];

// Create a mount point for express. / will be root for scripts.
app.use(express.static(__dirname + "/scripts"));

//Manage routes access
// Configure router to redirect to API. There API is this file.
const router = express.Router();

// Config views engine rendering to swig. Tells engine to render html files with swig module.
app.engine('html', swig.renderFile);
// Configure views extension to .html
app.set('view engine', 'html');
// Set mount point for views to current folder/views.
app.set('views', path.join(__dirname, 'views'))
// Now just render file with standard render method will return html from views folder via Swig.

// Return login page
router.get('/', (req, res) => { res.render('app'); });

// Return chat page
router.get('/chat', (req, res) => {
    // Get parameters from url.
    let login = req.query['login'];
    let room = req.query['room'];
    
    // User undefined.
    if (room == undefined || login == undefined) {
        res.redirect('404');
    } else {
        // Check if user already exists. If he does not exist, return error.
        if (users.filter(v => { return v.username == login && v.room == room }).length == 1) {
            res.render('chat', { login: login, room: room });
        } else {
            res.redirect('404');
        }
    }
});

// Load icon to print toast.
router.get('/favicon/:file', (req, res) => { res.sendFile(__dirname + '/favicon/programmer.svg') });

// Return error page
router.get('/404', (req, res) => { res.render('error404'); });

// Check at last if routes are not chat or root. If it is not and the response status is 404, return error page.
router.use('*', (req, res) => {
    res.status(404).redirect('404');
});

// Add router to path
app.use('/', router);

// Create connection on socket.
io.on('connection', (socket) => {

    // Login event.
    socket.on('login', (callback) => {
        let already = false;

        // Check if user already exists in users array.
        for (let i = 0; i < users.length; i++) {
            if (users[i].username == callback.user.username) {
                already = true;
                break;
            }
        }

        // Send existence of user
        socket.emit("userExists", already);

        // If user does not already exists, then push it to users array.
        if (!already) {
            users.push(callback.user);
        }
    });

    // Notify all users there is a nex user.
    socket.on('newUser_', (user) => socket.broadcast.emit('newUser', user));

    // Return all users already connected.
    socket.on('getUsers', () => { socket.emit('setUsers', JSON.stringify(users)); });

    // Return last messages in the chat.
    // @param number: number of messages to return.
    socket.on('getChat', (number) => {
        socket.emit("msg", JSON.stringify(messages.slice(messages.length - number, messages.length)));
    });

    // Add the new message to messages array and notify other there is a new message.
    socket.on('send', (callback) => { messages.push(callback); socket.broadcast.emit("msg", JSON.stringify([callback])); });

    // On user log out, remove it from users array.
    socket.on('userLogOut', (user) => {
        users = users.filter(v => { return v.username != user.username });
        socket.broadcast.emit('rmUser', user);
    });
});

// Listen port.
http.listen(PORT);
