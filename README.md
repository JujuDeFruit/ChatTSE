# ChatTSE
## High School
Télécom Saint-Étienne France
## Section
__FISE 2__ "Web Computing & Networks"
## Contributors
* Degrange Alexis
* Raynal Julien

## Aim:
This little app was developped to train on nodeJs, expressJs and socket.io.
User select a unique username and choose a room to chat with all other users in the same room.

## Dependencies :
    * express
    * nodemon
    * socket.io
    * path
    * swig

NB : use __"npm i"__ in folder
Then start serving : __"npm start"__

Concat commands (all commands in terminal in folder) : __"npm i & npm start"__ => You need to __install NodeJS__ before and __add npm to path__
(Install NodeJS : https://nodejs.org/en/ LTS version recommended)

If there is no error : your application is running on http://localhost:8080

We would like to thank Freepick for giving us its free icon : http://www.freepik.com.
